type Assignment = {
id: number,
weight: number,
filePath: String
}

type AssignmentStorage = {
id: number,
items: Assignment[]
}